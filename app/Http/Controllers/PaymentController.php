<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Configurations
use App\Http\Controllers\Midtrans\Config;

// Midtrans API Resources
use App\Http\Controllers\Midtrans\Transaction;

// Plumbing
use App\Http\Controllers\Midtrans\ApiRequestor;
use App\Http\Controllers\Midtrans\SnapApiRequestor;
use App\Http\Controllers\Midtrans\Notification;
use App\Http\Controllers\Midtrans\CoreApi;
use App\Http\Controllers\Midtrans\Snap;

// Sanitization
use App\Http\Controllers\Midtrans\Sanitizer;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    //
    public function getResult(Request $req){

        $item_list = array();
        $amount = 0;
        Config::$serverKey = 'SB-Mid-server-D5Jlz5lxeyBzoych2ZMm1Yih';
        if (!isset(Config::$serverKey)) {
            return "Please set your payment server key";
        }
        Config::$isSanitized = true;

        // Enable 3D-Secure
        Config::$is3ds = true;
        
        // Required
        $total_price = $req->post('total_price');
        $product_description = $req->post('product_description');
        $customer_name = $req->post('customer_name');
        $customer_phone = $req->post('customer_phone');
        $customer_email = $req->post('customer_email');

        if(!$total_price){ return response()->json(['status'=> '400', 'error' => 'Please complete the total price!'], 400); }
        if(!$product_description){ return response()->json(['status'=> '400', 'error' => 'Please complete the product description!'], 400); }
        if(!$customer_name){ return response()->json(['status'=> '400', 'error' => 'Please complete the customer name!'], 400); }
        if(!$customer_phone){ return response()->json(['status'=> '400', 'error' => 'Please complete the customer phone!'], 400); }
        if(!$customer_email){ return response()->json(['status'=> '400', 'error' => 'Please complete the customer email!'], 400); }

        

         $item_list[] = [
                'id' => "1234",
                'price' => $total_price,
                'quantity' => 1,
                'name' => $product_description
        ];

        $transaction_details = array(
            'order_id' => rand(),
            'gross_amount' => $total_price, // no decimal allowed for creditcard
        );


        // Optional
        $item_details = $item_list;

        // Optional
        $customer_details = array(
            'first_name'    => $customer_name,
            'last_name'     => "",
            'email'         => $customer_email,
            'phone'         => $customer_phone,
        );

        // Optional, remove this to display all available payment methods
        $enable_payments = array();

        // Fill transaction details
        $transaction = array(
            'enabled_payments' => $enable_payments,
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details,
            'item_details' => $item_details,
        );
        // return $transaction;
        try {
            $snapToken = Snap::getSnapToken($transaction);
            return response()->json($snapToken, 200);
        } catch (\Exception $e) {
            dd($e);
            return ['code' => 0 , 'message' => 'failed'];
        }

    }
}
